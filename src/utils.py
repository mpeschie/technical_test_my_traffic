#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 13 09:18:53 2021

@author: margauxpeschiera
"""
import pathlib
import pyspark.sql.functions as F

from pyspark.sql import SparkSession

path_to_src = pathlib.Path(__file__).parent.absolute()
path_to_csv_file = str(str(path_to_src) + '/data/train.csv')


def get_df(path=path_to_csv_file):
    '''
    Creates a spark df from a csv file

    Parameters
    ----------
    path : string
        Path to the csv file that we want to process.

    Returns
    -------
    df : DataFrame
        Spark df created from csv file.

    '''

    spark = SparkSession \
        .builder \
        .appName("Spark Session Taxi Data") \
        .getOrCreate()

    df = spark.read.format("csv").option("header", "true").load(path)

    return df


def clean_taxi_trip_df(taxi_trip_df):
    '''
    Cleans taxi trip data by removing inconsistent rows

    Parameters
    ----------
    taxi_trip_df : DataFrame
        Spark dataframe containing all rows.

    Returns
    -------
    taxi_trip_df : TYPE
        Spark dataframe without inconsistent rows.

    '''

    # removing rows where the pickup location and dropoff location are the same
    taxi_trip_df = taxi_trip_df.filter(
        (taxi_trip_df.pickup_longitude != taxi_trip_df.dropoff_longitude) |
        (taxi_trip_df.pickup_latitude != taxi_trip_df.dropoff_latitude))

    # removing rows where dropoff datetime is greater than pickup datetime
    taxi_trip_df = taxi_trip_df.filter(
        taxi_trip_df.pickup_datetime < taxi_trip_df.dropoff_datetime)

    # removing rows where trip duration is equal to zero
    taxi_trip_df = taxi_trip_df.filter(taxi_trip_df.trip_duration != 0)

    return taxi_trip_df


def get_distance(lat_p1, lon_p1, lat_p2, lon_p2):
    '''
    Calculates the great-circle distance (in km) between two
    GPS points p1 and p2
    https://en.wikipedia.org/wiki/Great-circle_distance#Formulae

    Parameters
    ----------
    lat_p1 : col
        latitude of the first location.
    lon_p1 : col
        longitude of the first location.
    lat_p2 : col
        latitude of the second location.
    lon_p2 : col
        longitude of the second location.

    Returns
    -------
    Column
        Column contianing the haversine distance between two locations.

    '''
    return F.acos(
        F.sin(F.radians(lat_p1)) * F.sin(F.radians(lat_p2)) +
        F.cos(F.radians(lat_p1)) * F.cos(F.radians(lat_p2)) *
        F.cos(F.radians(lon_p1) - F.radians(lon_p2))
    ) * F.lit(6371.0)


def get_segment(date_col, timerange):
    '''
    Get a column containing segments of 'timerange' hours

    Parameters
    ----------
    date_col : Column
        Column (containing a date) used to split into timerange segements.
    timerange : int
        Number of hours in each segment.

    Returns
    -------
    Column
        Column containing a number representing segment of the day.

    '''

    return (F.date_format(date_col, "HH") / timerange).cast('int')
