#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 09:05:39 2021

@author: margauxpeschiera
"""
import pathlib
import pytest

from datetime import datetime
from pyspark.sql import SparkSession
from pyspark_test import assert_pyspark_df_equal

from src.utils import get_df, clean_taxi_trip_df, get_distance, get_segment


@pytest.fixture()
def data():
    pickup_datetime = datetime(2020, 1, 1, 12, 50, 23)
    spark = SparkSession \
        .builder \
        .appName("Spark Session Test Taxi Data") \
        .getOrCreate()
    df = spark.createDataFrame(
        [    # witness
            (1, pickup_datetime, datetime(2020, 1, 2, 0, 0, 23),
             -73.982154846191406, 40.767936706542969,
             -73.964630126953125, 40.765602111816406, 450),

            # drop off dt < pickup dt
            (2, pickup_datetime, datetime(2020, 1, 1, 10, 50, 23),
             -73.980415344238281, 40.738563537597656,
             -73.999481201171875, 40.731151580810547, 663),

            # trip duration = 0
            (3, pickup_datetime, datetime(2020, 1, 1, 15, 50, 23),
             -73.979026794433594, 40.763938903808594,
             -74.005332946777344, 40.710086822509766, 0),

            # pickup datetime == dropoff datetime
            (4, pickup_datetime, datetime(2020, 1, 1, 21, 50, 23),
             -73.979026794433594, 40.763938903808594,
             -73.979026794433594, 40.763938903808594, 2124)

        ],
        ['id', 'pickup_datetime', 'dropoff_datetime', 'pickup_longitude',
         'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
         'trip_duration']
    )
    yield df


def test_get_df():
    path_to_src = pathlib.Path(__file__).parent.absolute()
    path_to_test_file = str(str(path_to_src) + '/test_data_utils.csv')

    df = get_df(path_to_test_file)

    assert df.count() == 4


def test_clean_df(data):
    df = clean_taxi_trip_df(data)

    assert df.count() == 1


def test_get_distance(data):
    df = data.withColumn("distance_in_km", get_distance(
        data.pickup_latitude, data.pickup_longitude, data.dropoff_latitude,
        data.dropoff_longitude))

    df = df.drop(*['pickup_datetime', 'dropoff_datetime', 'pickup_longitude',
                   'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
                   'trip_duration'])

    df = df.where(df.id != 4)

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    expected = spark.createDataFrame(
        [
            (1, 1.4985207804065126),
            (2, 1.8055071674659404),
            (3, 6.385098494753634)

        ],
        ['id', 'distance_in_km'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)


def test_get_segment(data):
    df = data.withColumn("segment_4h", get_segment(data.dropoff_datetime, 4))
    df = df.drop(*['pickup_datetime', 'dropoff_datetime', 'pickup_longitude',
                   'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
                   'trip_duration'])

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    expected = spark.createDataFrame(
        [
            (1, 0),
            (2, 2),
            (3, 3),
            (4, 5)

        ],
        ['id', 'segment_4h'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)
