#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 21:18:14 2021

@author: margauxpeschiera
"""

import pytest

from datetime import datetime
from pyspark.sql import SparkSession
from pyspark_test import assert_pyspark_df_equal

from src.metrics import get_mean_speed, get_count_trip_dow, \
    get_count_trip_fourh, get_nb_km_dow


@pytest.fixture()
def data():
    dropoff_datetime = datetime(2020, 3, 1, 12, 50, 23)
    spark = SparkSession \
        .builder \
        .appName("Spark Session Test Taxi Data") \
        .getOrCreate()
    df = spark.createDataFrame(
        [
            (1, datetime(2020, 2, 1, 11, 30, 20), dropoff_datetime,
             -70.982154846191406, 40.767936706542969,
             -72.964630126953125, 40.765602111816406, 5000),
            (2, datetime(2020, 3, 1, 12, 40, 54), dropoff_datetime,
             -73.980415344238281, 40.738563537597656,
             -73.999481201171875, 40.731151580810547, 663),
            (3, datetime(2020, 3, 1, 12, 30, 54), dropoff_datetime,
             -73.780415344238281, 40.738563537597656,
             -73.999481201171875, 40.731151580810547, 1200),
        ],
        ['id', 'pickup_datetime', 'dropoff_datetime', 'pickup_longitude',
         'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
         'trip_duration']
    )
    yield df


def test_get_mean_speed(data):
    df = get_mean_speed(data)

    df = df.drop(*['pickup_datetime', 'dropoff_datetime', 'pickup_longitude',
                   'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
                   'trip_duration', 'distance_in_km'])

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    expected = spark.createDataFrame(
        [
            (1, 120.20620112832086),
            (2, 9.80365882786936),
            (3, 55.42836757580831),

        ],
        ['id', 'mean_speed_kmh'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)


def test_get_count_trip_dow(data):
    df = get_count_trip_dow(data)

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    # Not exaustive
    expected = spark.createDataFrame(
        [
            ('Saturday', 1),
            ('Sunday', 2),

        ],
        ['week_day', 'count_of_trip'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)


def test_get_count_trip_fourh(data):
    df = get_count_trip_fourh(data)

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    # Not exaustive
    expected = spark.createDataFrame(
        [
            (3, 2),
            (2, 1),

        ],
        ['segment_4h', 'count_of_trip'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)


def test_nb_km_dow(data):
    df = get_nb_km_dow(data)

    spark = SparkSession \
        .builder \
        .appName("Spark Session Expected Taxi Data") \
        .getOrCreate()

    # Not exaustive
    expected = spark.createDataFrame(
        [
            ('Saturday', 166.95305712266784),
            ('Sunday', 20.281629692735375),

        ],
        ['week_day', 'sum_distance'])

    assert_pyspark_df_equal(df, expected, check_dtype=False)
