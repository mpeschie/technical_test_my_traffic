#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 13 09:19:58 2021

@author: margauxpeschiera
"""

import getopt
import pathlib
import sys

from pyspark.sql.functions import col, count, date_format, sum

from src.utils import get_distance, get_segment, get_df, clean_taxi_trip_df


def get_mean_speed(df):
    '''
    Add a column into df with the mean speed of the trip

    Parameters
    ----------
    df : DataFrame
        Spark dataframe with at least pickup/dropoff location and
        trip_duration.

    Returns
    -------
    df : DataFrame
        Spark dataframe with the added columns distance_in_km and
        mean_speed_kmh.

    '''

    df = df.withColumn("distance_in_km", get_distance(
        df.pickup_latitude, df.pickup_longitude, df.dropoff_latitude,
        df.dropoff_longitude))

    df = df.withColumn("mean_speed_kmh", df.distance_in_km /
                       (df.trip_duration / 3600))
    return df


def get_count_trip_dow(df):
    '''
    Count the number of trip for each day od the week

    Parameters
    ----------
    df : DataFrame
        Spark dataframe with at least pickupdate.

    Returns
    -------
    df : DataFrame
        Spark dataframe with a column week_day and a column count_of_trip
        containing the total number of trip for each day of the week.

    '''
    df = df.withColumn("week_day", date_format(col("pickup_datetime"), "EEEE"))

    df = df.groupBy(df.week_day).agg(count(df.week_day).alias("count_of_trip"))
    return df


def get_count_trip_fourh(df):
    '''
    Count the number of trip for each 4 hours of the day

    Parameters
    ----------
    df : DataFrame
        Spark dataframe with at least pickupdate.

    Returns
    -------
    df : DataFrame
        Spark dataframe with a column segment_4h and a column count_of_trip
        containing the total number of trip for each 4 hour of the day.
        (Total of 6 ranges)

    '''
    df = df.withColumn("segment_4h", get_segment("pickup_datetime", 4))

    df = df.groupBy(df.segment_4h).agg(
        count(df.segment_4h).alias("count_of_trip"))

    return df


def get_nb_km_dow(df):
    '''
    Count the number of km for each day of the week

    Parameters
    ----------
    df : DataFrame
        Spark dataframe with at least pickup/dropoff location and pickupdate.

    Returns
    -------
    df : DataFrame
        Spark dataframe with a column week_day and a column sum_distance
        containing the total number of km traveled for each day of the week.
    '''
    df = df.withColumn("distance_in_km", get_distance(
        df.pickup_latitude, df.pickup_longitude, df.dropoff_latitude,
        df.dropoff_longitude))

    df = df.withColumn("week_day", date_format(col("pickup_datetime"), "EEEE"))

    df = df.groupBy(df.week_day).agg(
        sum(df.distance_in_km).alias("sum_distance"))

    return df


def get_options():
    options = {}

    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "h", ["help", "mean_speed", "trip_dow", "trip_fourh",
                                "km_dow"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ["--mean_speed", "--trip_dow", "--trip_fourh", "--km_dow"]:
            options["action"] = opt
        elif opt in ("-h", "--help"):
            print(
                "You can precise : \n"
                "--mean_speed to get the mean speed of each trip\n"
                "--trip_dow to get the total number of trip in each different"
                "day of the week\n"
                "--trip_fourh to get the total number of trip in each 4 hours"
                "of the day\n"
                "--km_dow to get the total sum of km for each day of"
                "the week")
            sys.exit()
    return options


def get_result(action):

    path_to_src = pathlib.Path(__file__).parent.absolute()
    path_to_csv_file = str(str(path_to_src) + '/data/train.csv')

    df = clean_taxi_trip_df(get_df(path_to_csv_file))

    if action == "--mean_speed":
        df = get_mean_speed(df)
        df.show()
        return df
    elif action == "--trip_dow":
        df = get_count_trip_dow(df)
        df.show()
        return df
    elif action == "--trip_fourh":
        df = get_count_trip_fourh(df)
        df.show()
        return df
    elif action == "--km_dow":
        df = get_nb_km_dow(df)
        df.show()
        return df


def main():
    options = get_options()

    action = options.get("action")

    get_result(action)


if __name__ == '__main__':

    main()
