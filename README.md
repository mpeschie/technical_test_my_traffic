# Technical Test MyTraffic

## Requirements

- Have Python 3.7 or higher installed

## Launching the project

1) Clone the repository

```console 
$ git clone https://gitlab.utc.fr/mpeschie/technical_test_my_traffic.git
```

2) Open the cloned repository 

```console 
$ cd technical_test_my_traffic
```

3) Install requirements in virtual env

```console 
$ python3 -m venv venv_test_mytraffic 
$ source venv_test_mytraffic/bin/activate
$ pip3 install -r requirements.txt
```

## How to run the code and get the metrics 

If you got this error : ModuleNotFoundError: No module named 'src', please set the PYTHONPATH to technical_test_my_traffic directory.
```console
$ export PYTHONPATH='/Path/To/technical_test_my_traffic'
```
#### Get mean speed

```console
$ python3 src/metrics.py --mean_speed
```

This will print part of the df with the column mean_speed_kmh and return the df

#### Get count of trip for each day of the week

```console
$ python3 src/metrics.py --trip_dow
```

This will print a df with week_day and count_of_trip columns and return the df

#### Get count of trip for each 4h of the day

```console
$ python3 src/metrics.py --trip_fourh
```

This will print a df with segment_4h and count_of_trip columns and return the df

#### Get the total number of km for each day of the week

```console
$ python3 src/metrics.py --km_dow
```

This will print a df with week_day and sum_distance columns and return the df





